package ru.tsc.borisyuk.tm.api.controller;

public interface ICommandController {

    void exit();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrorCommand();

    void showErrorArgument();

}
