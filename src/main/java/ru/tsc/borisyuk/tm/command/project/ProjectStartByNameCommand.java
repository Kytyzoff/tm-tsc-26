package ru.tsc.borisyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by name...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startByName(userId, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
