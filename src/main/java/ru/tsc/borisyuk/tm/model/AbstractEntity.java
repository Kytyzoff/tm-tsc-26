package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;


import java.util.UUID;

@Getter
@Setter

public abstract class AbstractEntity {

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
